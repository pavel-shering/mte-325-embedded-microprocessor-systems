#include "basic_io.h"
#include "SD_Card.h"
#include "fat.h"
#include "Open_I2C.h"
#include "wm8731.h"
#include "LCD.h"
#include <stdio.h>

#include "alt_types.h"
#include <unistd.h>
#include "system.h"
#include "sys/alt_irq.h"
#include "altera_avalon_pio_regs.h"

#define STOP       14    // PB0
#define PLAY       13    // PB1
#define NEXT       11    // PB2
#define PREV        7    // PB3
#define NO_BUTTON  15

// defines for the buttons
#define NORMAL_SPEED    0
#define DOUBLE_SPEED    4
#define HALF_SPEED      5
#define DELAY_CHANNEL   6
#define REVERSE_PLAY    7

// defines for the display
#define NORMAL_SPEED_DISPLAY     0
#define DOUBLE_SPEED_DISPLAY     1
#define HALF_SPEED_DISPLAY       2
#define DELAY_CHANNEL_DISPLAY    3
#define REVERSE_PLAY_DISPLAY     4

#define TRUE     1
#define FALSE    0

#define SWICTH0     ((IORD(SWITCH_PIO_BASE, 0) >> 0) & 1)
#define SWICTH1     (((IORD(SWITCH_PIO_BASE, 0) >> 1) & 1) << 1)
#define SWICTH2     (((IORD(SWITCH_PIO_BASE, 0) >> 2) & 1) << 2)
#define WHICH_MODE() SWICTH0 + SWICTH1 + SWICTH2

volatile alt_u8 button_state;
volatile alt_u8 song_to_play = 0;

static BYTE init(void);
static void button_ISR(void *context, alt_u32 id);
static void play_song(data_file *df, alt_u8 MODE_DISPLAY, alt_u8 MODE);
static void init_buttons(void);

static void init_buttons(void) {
    // Direction is input only
    // Resest the edge capture register by writting to it (any value)
    alt_irq_register(BUTTON_PIO_IRQ, (void*)0, button_ISR);

    // Reset the edge capture register by writting to it
    IOWR(BUTTON_PIO_BASE, 3, 0x0);

    // Enable interrupts for all four buttons
    IOWR(BUTTON_PIO_BASE, 2, 0xf);

    printf("\nButtons Initialized\n");
}

static void button_ISR(void *context, alt_u32 id) {
    if ((IORD(BUTTON_PIO_BASE, 0) == 14) || (IORD(BUTTON_PIO_BASE, 0) == 13) || (IORD(BUTTON_PIO_BASE, 0) == 11) || (IORD(BUTTON_PIO_BASE, 0) == 7) || (IORD(BUTTON_PIO_BASE, 0) == 15)) {
        button_state = IORD(BUTTON_PIO_BASE, 0);
    }

    /* Reset the edge capture register to clear the interrupt */
    IOWR(BUTTON_PIO_BASE, 3, 0x0);
}

static BYTE init(void) {
    BYTE x = 0;
    x = SD_card_init();

    x = init_mbr();
    if (x != 0 ) {
        printf("\nMBR Initialization FAILED!");
        return x;
    }

    x = init_bs();
    if (x != 0 ) {
        printf("\nBS Initialization FAILED!");
        return x;
    }

    init_audio_codec();
    LCD_Init();
//    LCD_Test();
    init_buttons();
    return 0;
}


static void play_song(data_file *df, alt_u8 MODE_DISPLAY, alt_u8 MODE) {
    UINT32 length;
    UINT16 tmpL, tmpR;
    int i,j;
    alt_u8 speed = 4;
    if (MODE == DOUBLE_SPEED) speed = 8;

    file_number = song_to_play;

    LCD_File_Buffering(df -> Name);

    length = 1 + ceil(df->FileSize/(BPB_BytsPerSec*BPB_SecPerClus)); // file_size / bytes per cluster
    int *cc = (int *)malloc(sizeof(int)*length);

    BYTE *buffer = (BYTE *)malloc(sizeof(BYTE)* BPB_BytsPerSec);
    memset(buffer, 0, sizeof(BYTE)* BPB_BytsPerSec);
    build_cluster_chain(cc, length, df);

    LCD_Display(df->Name, MODE_DISPLAY);

    if((MODE != REVERSE_PLAY) && (MODE != DELAY_CHANNEL)) {
        for(i = 0; i < BPB_SecPerClus * length; i++) {
            if (button_state == STOP) return;

            get_rel_sector(df, buffer, cc, i);
            for(j = 0; j < BPB_BytsPerSec; j += speed) {
                if (button_state == STOP) return;

                while(IORD( AUD_FULL_BASE, 0 )) {} //wait until the FIFO is not full
                tmpL = ( buffer[ j + 1 ] << 8 ) | ( buffer[ j ] ); //Package 2 8-bit bytes from the sector buffer array into the
                IOWR(AUDIO_0_BASE, 0, tmpL); //Write the 16-bit variable tmp to the FIFO where it will be processed by the audio CODEC

                while(IORD( AUD_FULL_BASE, 0 )) {} 
                tmpR = ( buffer[ j + 3 ] << 8 ) | ( buffer[ j + 2 ] ); 
                IOWR(AUDIO_0_BASE, 0, tmpR); 

                if(MODE == HALF_SPEED) {


                    while(IORD( AUD_FULL_BASE, 0 )) {} 
                    IOWR(AUDIO_0_BASE, 0, tmpL); 

                    while(IORD( AUD_FULL_BASE, 0 )) {} 
                    IOWR(AUDIO_0_BASE, 0, tmpR); 
                }
            }
        }
    } else if (MODE == REVERSE_PLAY) {
        for(i = BPB_SecPerClus * length ; i >= 0; i--) {
            if (button_state == STOP) return;
            
            get_rel_sector(df, buffer, cc, i);
            for(j = BPB_BytsPerSec -4; j >=0 ; j -= speed) {
                if (button_state == STOP){
                    return;
                }
                while(IORD( AUD_FULL_BASE, 0 )) {};
                tmpL = ( buffer[ j + 1 ] << 8 ) | ( buffer[ j ] ); 
                IOWR(AUDIO_0_BASE, 0, tmpL); 

                while(IORD( AUD_FULL_BASE, 0 )) {};
                tmpR = ( buffer[ j + 3 ] << 8 ) | ( buffer[ j + 2 ] ); 
                IOWR(AUDIO_0_BASE, 0, tmpR); 
            }
        }
    } else if (MODE == DELAY_CHANNEL){
        alt_u32 countRight = 0;
        // create a buffer for 1 second worth of data for one of the channels
        alt_u16 *bufferRight = (alt_u16 *)malloc(sizeof(alt_u16)*44100);
        memset(bufferRight, 0, 88200);

        for(i = 0; i < BPB_SecPerClus * length; i++) {
            if (button_state == STOP) return;
        
            get_rel_sector(df, buffer, cc, i);
            for(j = 0; j < BPB_BytsPerSec; j += speed) {
                if (button_state == STOP) return;
                
                //play left
                while(IORD( AUD_FULL_BASE, 0 )) {};
                tmpL = ( buffer[ j + 1 ] << 8 ) | ( buffer[ j ] );
                IOWR(AUDIO_0_BASE, 0, tmpL);

                //play right
                while(IORD( AUD_FULL_BASE, 0 )) {};
                IOWR(AUDIO_0_BASE, 0, bufferRight[countRight] );

                //store right
                bufferRight[countRight] = ( buffer[ j + 3 ] << 8 ) | ( buffer[ j + 2 ] );
                if(countRight == 44100) {
                    countRight = 0;
                } else {
                    countRight++;
                }
            }
        }
        //play the rest of the delayed channel
        for(i = 0; i < 44100; i++) {
            if (button_state == STOP) return;
            
            //play left
            while(IORD( AUD_FULL_BASE, 0 )) {}
            IOWR(AUDIO_0_BASE, 0, 0);

            //play right
            while(IORD( AUD_FULL_BASE, 0 )) {} 
            IOWR(AUDIO_0_BASE, 0, bufferRight[i]);
        }
    }
}

int main() {
    BYTE result = init();
    if(result != 0) {
        printf("\nInitializations FAILED!");
        return 0;
    }

    info_bs();
    data_file *df2 = (data_file *)malloc(sizeof(data_file));


    if (search_for_filetype("WAV", df2, 0, 1) == 1) {
        printf("file not found!");
    }

    alt_u8 old_mode = WHICH_MODE();
    alt_u8 old_song = 255;

    while (1) {
        printf("\nMODE: %d", WHICH_MODE());

        while(button_state != PLAY) {
            if (button_state == NEXT) {
                song_to_play += 1;
                if (song_to_play >= 13) {
                    song_to_play = 0;
                }
                while (button_state != NO_BUTTON) {};
                printf("\nSONG TO PLAY: %d \n", song_to_play);

                file_number = song_to_play;

                if (search_for_filetype("WAV", df2, 0, 1) == 1) {
                    printf("file not found!");
                }
            }

            if (button_state == PREV) {
                if (song_to_play <= 0) {
                    song_to_play = 12;
                } else {
                    song_to_play -= 1;
                }

                while (button_state != NO_BUTTON) {};

                file_number = song_to_play;

                if (search_for_filetype("WAV", df2, 0, 1) == 1) {
                    printf("file not found!");
                }

            }
            //printf("\nFILE NUMBER: %d", file_number);

            if ((old_mode != WHICH_MODE()) || (old_song != song_to_play)) {

                old_mode = WHICH_MODE();
                old_song = song_to_play;

                switch(WHICH_MODE()) {
                    case NORMAL_SPEED:
                        LCD_Display(df2->Name, NORMAL_SPEED_DISPLAY);
                        break;

                    case DOUBLE_SPEED:
                        LCD_Display(df2->Name, DOUBLE_SPEED_DISPLAY);
                        break;

                    case HALF_SPEED:
                        LCD_Display(df2->Name, HALF_SPEED_DISPLAY);
                        break;

                    case DELAY_CHANNEL:
                        LCD_Display(df2->Name, DELAY_CHANNEL_DISPLAY);
                        break;

                    case REVERSE_PLAY:
                        LCD_Display(df2->Name, REVERSE_PLAY_DISPLAY);
                        break;

                    default:
                        LCD_Display(df2->Name, NORMAL_SPEED_DISPLAY);
                        break;
                }
            }
        }

        switch(WHICH_MODE()) {
            case NORMAL_SPEED:
                play_song(df2, NORMAL_SPEED_DISPLAY, NORMAL_SPEED);
                break;

            case DOUBLE_SPEED:
                play_song(df2, DOUBLE_SPEED_DISPLAY, DOUBLE_SPEED);
                break;

            case HALF_SPEED:
                play_song(df2, HALF_SPEED_DISPLAY, HALF_SPEED);
                break;

            case DELAY_CHANNEL:
                play_song(df2, DELAY_CHANNEL_DISPLAY, DELAY_CHANNEL);
                break;

            case REVERSE_PLAY:
                play_song(df2, REVERSE_PLAY_DISPLAY, REVERSE_PLAY);
                break;

            default:
                play_song(df2, NORMAL_SPEED_DISPLAY, NORMAL_SPEED);
                break;
        }
    }

    return 0;
}
