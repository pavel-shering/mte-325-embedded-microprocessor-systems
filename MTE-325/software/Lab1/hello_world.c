/*
 * "Hello World" example.
 *
 * This example prints 'Hello from Nios II' to the STDOUT stream. It runs on
 * the Nios II 'standard', 'full_featured', 'fast', and 'low_cost' example
 * designs. It runs with or without the MicroC/OS-II RTOS and requires a STDOUT
 * device in your system's hardware.
 * The memory footprint of this hosted application is ~69 kbytes by default
 * using the standard reference design.
 *
 * For a reduced footprint version of this template, and an explanation of how
 * to reduce the memory footprint for a given application, see the
 * "small_hello_world" template.
 *
 */
#include "alt_types.h"
#include <stdio.h>
#include <unistd.h>
#include "system.h"
#include "sys/alt_irq.h"
#include "altera_avalon_pio_regs.h"

//global variables
volatile alt_u8 count_flag = 0x0;
volatile alt_u8 led_state = 0x0;
volatile alt_u8 led_state2 = 0x81;
volatile alt_u8 isPhase1 = 0;
volatile alt_u8 isInterrupt = 0;
volatile alt_u8 egm_pulse = 0;
volatile alt_u8 responseHigh = 0;

volatile alt_u8 buttonPressed;
volatile alt_u8 switches_button0;
volatile alt_u8 switches_button1;

// 32 bit period is used for the timer
alt_u32 timerPeriod = TIMER_0_FREQ;

// prototypes
static void init_EGM(void);
static void EGM_ISR(void *context, alt_u32 id);
static void button_ISR( void *context, alt_u32 id);
static void TIMER_0_ISR(void *context, alt_u32 id);
static void TIMER_1_ISR(void *context, alt_u32 id);
static void init_timer0(void);
static void init_timer1(void);

static void button_ISR( void *context, alt_u32 id) {
	buttonPressed = IORD(BUTTON_PIO_BASE,0);

	// key 0 button == 14
	// key 1 button == 13
	if (buttonPressed == 14) {
		switches_button0 = IORD(SWITCH_PIO_BASE,0);

		// stop timer
		IOWR(TIMER_0_BASE, 1, 0x8);

		// clear the register count
		IOWR(TIMER_0_BASE, 2, (alt_u16)timerPeriod);

		// start timer
		IOWR(TIMER_0_BASE, 1, 0x7);

		//set the flag with a non zero value
		if (switches_button0 %2) {
			IOWR(LED_PIO_BASE, 0, 0x2); // on
		} else {
			IOWR(LED_PIO_BASE, 0, 0x0); // off
		}
		switches_button0 >>= 1;
	}
	else if(buttonPressed == 13) {
		switches_button1 = IORD(SWITCH_PIO_BASE,0);
		IOWR(TIMER_1_BASE, 1, 0x8);

		// clear the register count
		IOWR(TIMER_1_BASE, 2, (alt_u16)timerPeriod);

		// start timer
		IOWR(TIMER_1_BASE, 1, 0x7);

		//set the flag with a non zero value
		if (switches_button1 %2) {
			IOWR(SEVEN_SEGMENT_MIDDLE_PIO_BASE, 0, 0xcf); // 1
		} else {
			IOWR(SEVEN_SEGMENT_MIDDLE_PIO_BASE, 0, 0x81); // 0
		}
		switches_button1 >>= 1;
	}

	/* Reset the edge capture register to clear the interrupt */
	IOWR(BUTTON_PIO_BASE, 3, 0x0);
}

static void TIMER_0_ISR(void *context, alt_u32 id) {
	if (switches_button0 % 2) {
		led_state = 0x2;
	} else {
		led_state = 0x0;
	}
	switches_button0 >>= 1;

	//set the flag with a non zero value
	IOWR(LED_PIO_BASE, 0, led_state);

	// turn off the timer after 8 sec
	if (switches_button0 == 0) {
		IOWR(TIMER_0_BASE, 1, 0x8);
		IOWR(LED_PIO_BASE, 0, 0x0);
	}

	//Acknowledge the interrupt by clearing the TO bit in the status register
	IOWR(TIMER_0_BASE, 0, 0x0);
}

static void TIMER_1_ISR(void *context, alt_u32 id) {
	//set the flag with a non zero value
	if (switches_button1 % 2) {
		led_state2 = 0xcf;
	} else {
		led_state2 = 0x81;
	}

	switches_button1 >>= 1;
	IOWR(SEVEN_SEGMENT_MIDDLE_PIO_BASE, 0, led_state2);

	// turn off the timer after 8 sec
	if (switches_button1 == 0) {
		IOWR(TIMER_1_BASE, 1, 0x8);
		IOWR(SEVEN_SEGMENT_MIDDLE_PIO_BASE, 0, 0x81);
	}

	//Acknowledge the interrupt by clearing the TO bit in the status register
	IOWR(TIMER_1_BASE, 0, 0x0);
}

static void EGM_ISR(void *context, alt_u32 id) {
	//respond to the pulse
	if(IORD(PIO_PULSE_BASE, 0) == 1) {
		IOWR(PIO_RESPONSE_BASE, 0, 1);
		egm_pulse++;
	} else {
		IOWR(PIO_RESPONSE_BASE, 0, 0);
	}

	// clear the interrupt
	IOWR(PIO_PULSE_BASE, 3, 0x0);
}

static void init_timer0(void) {
	//Set timer 0 period
	IOWR(TIMER_0_BASE, 2, (alt_u16)timerPeriod);
	IOWR(TIMER_0_BASE, 3, (alt_u16)(timerPeriod >> 16));

	//Initialise timer interrupt vector
	alt_irq_register(TIMER_0_IRQ, (void *)0, TIMER_0_ISR);

	//Clear timer interrupt bit in status register
	IOWR(TIMER_0_BASE, 0, 0x0);

	//Initialise timer control - start timer, run continuously, enable interrupts
	printf("Timer0 Initialised\n");
}


static void init_timer1(void) {
	IOWR(TIMER_1_BASE, 2, (alt_u16)timerPeriod);
	IOWR(TIMER_1_BASE, 3, (alt_u16)(timerPeriod >> 16));
	alt_irq_register(TIMER_1_IRQ, (void *)0, TIMER_1_ISR);
	IOWR(TIMER_1_BASE, 0, 0x0);

	printf("Timer1 Initialised\n");
}

static void init_buttons(void) {
	/* Direction is input only */
	/* Reset the edge capture register by writing to it (any value will do) */
	alt_irq_register( BUTTON_PIO_IRQ, (void*)0, button_ISR );

	/* Reset the edge capture register by writing to it (any value will do) */
	IOWR(BUTTON_PIO_BASE, 3, 0x0);

	/* Enable interrupts for all four buttons*/
	IOWR(BUTTON_PIO_BASE, 2, 0xf);

	printf("Buttons Initialised\n");
}

static void init_EGM(void) {
	/* Reset the edge capture register by writing to it (any value will do) */
	alt_irq_register(PIO_PULSE_IRQ, (void*)0, EGM_ISR);
	/* Reset the edge capture register by writing to it (any value will do) */
	IOWR(PIO_PULSE_BASE, 3, 0x0);
	/* Enable interrupts */
	IOWR(PIO_PULSE_BASE, 2, 0xf);
}

int main() {
	volatile alt_u8 egm_period = 1;
	volatile alt_u8 egm_duty_cycle = 1;

	//check to see if phase 1 is to be run (switch 15)
	//DOWN is phase 1, UP is phase 2
	isPhase1 = 1 - ((IORD(SWITCH_PIO_BASE,0) >> 15) % 2);

	//check switch 14 to see if interrupt or occasional polling is to be used
	//DOWN is occasional, UP is interrupt
	isInterrupt = ((IORD(SWITCH_PIO_BASE,0) >> 14) % 2);

	printf("Hello from Nios II!\n");

	if(isPhase1) {
		printf("Running Phase1\n");
	} else {
		printf("Running Phase2\n");
		const char* scheduling = "";
		if(isInterrupt){
			scheduling = "Running Interrupt Scheduling tests";
		} else {
			scheduling = "Running Occasional Scheduling tests";
		}
		printf("%s\n", scheduling);
	}

	if (isPhase1){//phase 1 code
		init_timer0();
		init_timer1();
		init_buttons();

		while(1){}
	}
	else{//phase 2 code
		printf("period, dutyCycle, granularity, missed, latency, tasks\n");
		if (isInterrupt == 0){ //occasional polling
			int i,j,k, l;
			for (i = egm_period; i <= 14; i++) { // period
				for (j = egm_duty_cycle; j <= 14; j++) { // duty cycle
					for(k = 1; k <= 201; k += 20) { // granularity
						for (l = 0; l <= 5; l++) { // statistical analysis
							init(i, j);
							while(egm_pulse <= 100) {
								background(k);
								if(responseHigh == 1) {
									if(IORD(PIO_PULSE_BASE, 0) == 0) {
										IOWR(PIO_RESPONSE_BASE, 0, 0);
										responseHigh = 0;
									}
								} else {
									if(IORD(PIO_PULSE_BASE, 0) == 1){
										IOWR(PIO_RESPONSE_BASE, 0, 1);
										egm_pulse ++;
										responseHigh = 1;
									}
								}
							}
							finalize(k);
							egm_pulse = 0;
						}
					}
				}
			}
			printf("done occasional");
		} else { //interrupt
			int i,j,k,l;
			egm_period = 2;
			egm_duty_cycle = 2;
			for (i = egm_period; i <= 14; i++) { // period
				for (j = egm_duty_cycle; j <= 14; j++) { //duty cycle
					for(k = 1; k <= 201; k += 20) { // granularity
						for (l = 0; l <= 5; l++) { // statistical analysis
							init_EGM();
							init(i, j);
								while(egm_pulse < 100) {
									background(k);
								}
								finalize(k);
								egm_pulse = 0;
						}
					}
				}
			}
			printf("done interrupt");
		}
	}
	return 0;
}