/*
 * "Hello World" example.
 *
 * This example prints 'Hello from Nios II' to the STDOUT stream. It runs on
 * the Nios II 'standard', 'full_featured', 'fast', and 'low_cost' example
 * designs. It runs with or without the MicroC/OS-II RTOS and requires a STDOUT
 * device in your system's hardware.
 * The memory footprint of this hosted application is ~69 kbytes by default
 * using the standard reference design.
 *
 * For a reduced footprint version of this template, and an explanation of how
 * to reduce the memory footprint for a given application, see the
 * "small_hello_world" template.
 *
 */
#include "alt_types.h"
#include <stdio.h>
#include <unistd.h>
#include "system.h"
#include "sys/alt_irq.h"
#include "altera_avalon_pio_regs.h"

volatile alt_u8 count_flag = 0x0;
volatile alt_u8 led_state = 0x0;
// 32 bit period is used for the timer
alt_u32 timerPeriod = TIMER_0_FREQ;

static void TIMER_0_ISR(void *context, alt_u32 id);
static void init_timers(void);

static void TIMER_0_ISR(void *context, alt_u32 id) {
	//set the flag with a non zero value
	count_flag = 0xf;

	//Acknowledge the interrupt by clearing the TO bit in the status register
	IOWR(TIMER_0_BASE, 0, 0x0);
}

static void init_timers(void) {
	//Set timer period
	IOWR(TIMER_0_BASE, 2, (alt_u16)timerPeriod);
	IOWR(TIMER_0_BASE, 3, (alt_u16)timerPeriod >> 16);

	//Initialise timer interrupt vector
	alt_irq_register(TIMER_0_IRQ, (void *)0, TIMER_0_ISR);

	//Clear timer interrupt bit in status register
	IOWR(TIMER_0_BASE, 0, 0x0);

	//Initialise timer control - start timer, run continuously, enable interrupts
	IOWR(TIMER_0_BASE, 1, 0x7);

	// for code not to change use constants defined for each bit and OR them
//		IOWR_ALTERA_AVALON_TIMER_CONTROL(TIMER_0_BASE,
//			ALTERA_AVALON_TIMER_CONTROL_ITO_MSK |
//			ALTERA_AVALON_TIMER_CONTROL_CONT_MSK |
//			ALTERA_AVALON_TIMER_CONTROL_START_MSK);
}

int main()
{
	int delay;
	printf("Hello from Nios II!\n");
	printf("count_flag = %#08x\n", count_flag);
	printf("led_state = %#08x\n", led_state);
//	IOWR(LED_PIO_BASE, 0, 0xf);
	init_timers();

	while(1) {
		if(count_flag) {
			printf("count_flag = %#08x\n", count_flag);
			led_state ^= 0xf;
			IOWR(LED_PIO_BASE, 0, led_state);
			count_flag = 0x0;
		}

//		IOWR(LED_PIO_BASE, 0, 0);

//		printf("led_state = %#08x\n", led_state);
//		int temp = 0;
//		temp = IORD(BUTTON_PIO_BASE,0);
//		IOWR(GREEN_LED_PIO_BASE,0,temp);
	}
	return 0;
}
